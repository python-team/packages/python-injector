python-injector (0.21.0-1) unstable; urgency=medium

  * New upstream release.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 07 Apr 2024 17:33:13 +0200

python-injector (0.20.1-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.6.2. No changes needed.
  * debian/copyright:
    + Update copyright attributions for debian/.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 27 Feb 2023 11:01:48 +0100

python-injector (0.19.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove unnecessary get-orig-source-target.

  [ Mike Gabriel ]
  * Re-add python3-typing-extensions to Depends. This got log
    in upload of 0.19.0-1 (which was uploaded before pulling
    in team upload changes from the packaging Git). Also weave-in
    changelog stanza of release 0.18.4-2.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 16 Apr 2022 07:53:07 +0200

python-injector (0.19.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 15 Apr 2022 22:39:25 +0200

python-injector (0.18.4-2) unstable; urgency=medium

  * Team Upload.
  * Add python3-typing-extensions to Depends (Closes: #975090)

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 19 Nov 2020 16:42:01 +0530

python-injector (0.18.4-1) unstable; urgency=medium

  [ Mike Gabriel ]
  * New upstream release.
  * d/control: Bump to DH compat level version 13.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 26 Oct 2020 23:01:35 +0100

python-injector (0.18.3-2) unstable; urgency=medium

  * Source-only upload as is.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 23 Jun 2020 16:36:58 +0200

python-injector (0.18.3-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #958857).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Apr 2020 23:45:35 +0200
